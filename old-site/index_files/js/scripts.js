// JavaScript for themezinho
(function($) {
$(document).ready(function() {
	$(".video").click(function() {
		$.fancybox({
			'padding'		: 0,
			'autoScale'		: false,
			'transitionIn'	: 'none',
			'transitionOut'	: 'none',
			'title'			: this.title,
			'width'			: 640,
			'height'		: 385,
			'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
			'type'			: 'swf',
			'swf'			: {
			'wmode'				: 'transparent',
			'allowfullscreen'	: 'true'
			}
		});

		return false;
	});
	
	"use strict";
	
	
	// FANCYBOX
	$(".fancybox").fancybox({
  	helpers: {
    overlay: {
      locked: false
    	}
	  }
	});
	
	
	// Slick carousel
	$('.single-testimonial').slick({
		 dots: true,
		 autoplay:true,
		 arrows:false
		});

	// PARALLAX EFFECT
		$.stellar({
			horizontalScrolling: false,
			verticalOffset: 0,
			responsive:true
		});
	
		
	// SCROLL OPACITY
		var divs = $('.home-static-image .inner');
		var divs = $('.project-hero .inner');
		$(window).on('scroll', function() {
		var st = $(this).scrollTop();
		divs.css({ 'opacity' : (1 - st/400) });
		});
		
		
	
	// PAGE TRANSITION
		$('.transition').on('click', function(e) {
      	$('.transition-overlay').toggleClass("show-me");
      	$('body').toggleClass("overflow-hidden");
      	$("main").removeClass("move-down");
		$(".top-menu").removeClass("move-down");
		});
		
		
	// TRANSITION DELAY
		$('.transition').on('click', function(e) {
    	e.preventDefault();                  
    	var goTo = this.getAttribute("href"); 
		setTimeout(function(){
         window.location = goTo;
    	},1000);       
		}); 
		
	
	// MASONRY
		$('.ahome-portfolio ul').isotope({
			itemSelector: '.ahome-portfolio ul li',
			masonry: {
			  columnWidth: 0
			}
		});
	
	
	// SANDWICH MENU
		$('.sandwich-menu').on('click', function(e) {
		$(".sandwich-menu").toggleClass("menu-open");
		$("main").toggleClass("move-down");
		$(".top-menu").toggleClass("move-down");
		});
	
	
	}); 
	
	// WOW ANIMATION
	wow = new WOW(
      	{
       		animateClass: 'animated',
        	offset:       100
      	}
    	);
    	wow.init();
	
})(jQuery);